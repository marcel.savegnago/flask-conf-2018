from time import sleep
import mongoengine as me
from core import utils


class XkcdIndex(me.Document):
    xkcd_id = me.IntField(unique=True)
    title = me.StringField()
    comic_url = me.URLField()
    img_url = me.URLField()


def connect_to_database(connect_fn, db_conf, utils=utils):
    attempts = 0
    logger = utils.create_logger()
    backoff = lambda t: sleep(2 ** t)

    while attempts < 5:
        try:
            logger.info(f'Connecting to database...')
            return connect_fn(**db_conf)

        except Exception as err:
            attempts += 1
            logger.error(
                f'Unable to connect to db ({attempts}/5): {str(err)}')

            backoff(attempts)

    logger.error(
        'Failed to connect to database: Max attempts exceeded. Exiting...')
    exit(1)
