from random import choice

from flask import Blueprint, request, render_template
from mongoengine.errors import FieldDoesNotExist

from core import utils
from core.db import XkcdIndex


def create_routes(XkcIndex=XkcdIndex, request=request):
    api = Blueprint('api', __name__)

    @api.route('/api/comics',
               endpoint='handle_comics', methods=['GET', 'POST'])
    @handle_exceptions
    def handle_comics():
        if request.method == 'GET':
            return XkcdIndex.objects.to_json(), 200

        elif request.method == 'POST':
            XkcdIndex(**request.get_json()).save()
            return '', 201

    @api.route('/api/comics/<xkcd_id>', endpoint='get_comic')
    @handle_exceptions
    def get_comic_by_id(xkcd_id):
        comic = XkcdIndex.objects.get(xkcd_id=xkcd_id)
        return render_template('index.html', xkcd_id=comic.xkcd_id,
                               title=comic.title, img_url=comic.img_url,
                               comic_url=comic.comic_url)

    @api.route('/api/comics/random', endpoint='random_comic')
    @handle_exceptions
    def random_comic():
        comic = choice(XkcdIndex.objects.all())
        return render_template('index.html', xkcd_id=comic.xkcd_id,
                               title=comic.title, img_url=comic.img_url,
                               comic_url=comic.comic_url)

    return api


def handle_exceptions(fn):
    def wrapped(*args, **kwargs):
        try:
            return fn(*args, **kwargs)

        except FieldDoesNotExist:
            return 'Invalid json payload', 400

        except Exception as err:
            logger = utils.create_logger()
            logger.error(f'failed on {fn.__name__}: {str(err)}')
            return str(err), 500

    return wrapped
