import requests

base_url = 'http://localhost:8080/api/comics'

comics = [
    {
        'xkcd_id': 224,
        'title': 'LISP',
        'comic_url': 'https://www.xkcd.com/224/',
        'img_url': 'https://imgs.xkcd.com/comics/lisp.jpg'
    },
    {
        'xkcd_id': 1988,
        'title': 'Containers',
        'comic_url': 'https://xkcd.com/1988/',
        'img_url': 'https://imgs.xkcd.com/comics/containers.png'
    },
    {
        'xkcd_id': 538,
        'title': 'Security',
        'comic_url': 'https://www.xkcd.com/538/',
        'img_url': 'https://imgs.xkcd.com/comics/security.png'
    },
    {
        'xkcd_id': 327,
        'title': 'Exploits of a mom',
        'comic_url': 'https://www.xkcd.com/327/',
        'img_url': 'https://imgs.xkcd.com/comics/exploits_of_a_mom.png'
    },
    {
        'xkcd_id': 371,
        'title': 'Compiler complaint',
        'comic_url': 'https://xkcd.com/371/',
        'img_url': 'https://imgs.xkcd.com/comics/compiler_complaint.png'
    }
]

for comic in comics:
    requests.post(base_url, json=comic)
