build:
	docker build -t felipemocruha/xkcd-comics:$(APP_VERSION) -f deploy/Dockerfile .

push:
	docker push felipemocruha/xkcd-comics:$(APP_VERSION)

run_docker:
	cd deploy && docker-compose up -d

stop_docker:
	cd deploy && docker-compose down
